//
//  SceneDelegate.swift
//  iOS-Code-Challenge
//
//  Created by Mohit Gupta on 21/05/1942 Saka.
//  Copyright © 1942 Mohit Gupta. All rights reserved.
//

import Foundation
import UIKit

var container: UIView = UIView()
var loadingView: UIView = UIView()
var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
let loadingLabel = UILabel()

extension UIViewController {
    
    func showLoader() {
        container.frame = self.view.frame
        container.center = self.view.center
        container.backgroundColor = UIColor(white: 0, alpha: 0.3)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        loadingView.center = self.view.center
        loadingView.backgroundColor = .clear
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        loadingLabel.frame = CGRect(x: 0, y: 80, width: 120, height: 40)
        loadingLabel.text = "Loading..."
        loadingLabel.textAlignment = .center
        loadingLabel.textColor = .white
        
        
        container.addSubview(loadingView)
        loadingView.addSubview(activityIndicator)
        loadingView.addSubview(loadingLabel)
        UIApplication.shared.keyWindow?.addSubview(container)
        
        DispatchQueue.main.async {
            activityIndicator.startAnimating()
        }
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.removeFromSuperview()
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
            loadingLabel.removeFromSuperview()
            loadingView.removeFromSuperview()
            container.removeFromSuperview()
        }
    }
    
    
}
