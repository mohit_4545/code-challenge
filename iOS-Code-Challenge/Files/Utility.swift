 //
 //  SceneDelegate.swift
 //  iOS-Code-Challenge
 //
 //  Created by Mohit Gupta on 21/05/1942 Saka.
 //  Copyright © 1942 Mohit Gupta. All rights reserved.
 //
 
 import UIKit
 
 
 class Utility: NSObject {
    //Present Alert controller with title, message and events
    static var alert = UIAlertController()
    static func alertContoller(title: String, message: String, actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, firstActoin: Selector?,secondActoin: Selector?,thirdActoin: Selector?, controller: UIViewController) {
        alert = UIAlertController(title: title, message: message, preferredStyle: .alert) // 1
        if(!actionTitleFirst.isEmpty) {
            let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
                if(firstActoin != nil){
                    controller.perform(firstActoin)
                }
            }
            alert.addAction(firstButtonAction)
        }
        
        if(!actionTitleSecond.isEmpty) {
            let secondAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
                if(secondActoin != nil){
                    controller.perform(secondActoin)
                }
            }
            alert.addAction(secondAction)
        }
        
        if(!actionTitleThird.isEmpty) {
            let thirdAction = UIAlertAction(title: actionTitleThird, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
            }
            alert.addAction(thirdAction)
        }
        controller.present(alert, animated: true, completion:nil)
    }
    
    //Present Alert with title and message
    static func alert(message: String, title: String ,controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion:nil)
    }
    
    static func isNetworkAvailable(controller: UIViewController) -> Bool{
        if !Reachability.isConnectedToNetwork(){
            Utility.alert(message: "Please check your network connection.", title: "", controller: controller)
            return false
        }else{
            return true
        }
    }
    
    
 }
 
 
 
 
 
 @IBDesignable
 class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var VshadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = VshadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
 }
 
 
 
 
 
 
 extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
 }
