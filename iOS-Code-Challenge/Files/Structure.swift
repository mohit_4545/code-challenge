 //
 //  SceneDelegate.swift
 //  iOS-Code-Challenge
 //
 //  Created by Mohit Gupta on 21/05/1942 Saka.
 //  Copyright © 1942 Mohit Gupta. All rights reserved.
 //
 
 import Foundation
 import UIKit
 import RealmSwift
 
 struct ItemObject: Codable {
    let id: String?
    let type: String?
    let date: String?
    let data: String?
    //    init(from decoder: Decoder) throws {
    //        let container = try decoder.container(keyedBy: CodingKeys.self)
    //        self.id = try container.decode(String.self, forKey: .id)
    //        self.type = try container.decode(String.self, forKey: .type)
    //        self.date = try container.decode(String.self, forKey: .date)
    //        self.data = try container.decode(String.self, forKey: .data)
    //    }
    
 }
 
 
 
 // using for realm
 
 class ItemObj: Object {
    @objc dynamic var id = ""
    @objc dynamic var type = ""
    @objc dynamic var date = ""
    @objc dynamic var data = ""
    
    static func create(withId id: String,withType type: String,withDate date: String,withData data: String) -> ItemObj {
        let CurrentObj = ItemObj()
        CurrentObj.id = id
        CurrentObj.type = type
        CurrentObj.date = date
        CurrentObj.data = data
        return CurrentObj
    }
 }
 
 
 
 class Item: Object {
    
    var objectArr = List<ItemObj>()
    
    static func create(
        ItemObj: [ItemObj]) -> Item {
        let store = Item()
        
        store.objectArr.append(objectsIn: ItemObj)
        
        return store
        
    }
 }
