 //
 //  SceneDelegate.swift
 //  iOS-Code-Challenge
 //
 //  Created by Mohit Gupta on 21/05/1942 Saka.
 //  Copyright © 1942 Mohit Gupta. All rights reserved.
 //
 import UIKit
 import RealmSwift
 class ViewController: UIViewController{
    
    let realm = try! Realm()
    private var itemRecords = [ItemObject]()
    let TableView = UITableView()
    let rest = RestManager()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.leftBarButtonItem  = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sort))
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Online", style: .plain, target: self, action: #selector(onlineOffline))
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTable()
        
        navigationItem.title = "Items"
        if Utility.isNetworkAvailable(controller: self){
            getUsersList()
        }
    }
    
    
    
 }
 extension ViewController{
    // Basic configure method
    
    
    @objc func onlineOffline(sender: UIBarButtonItem){
        if sender.title == "Online"{
            sender.title = "Offline"
            self.read()
            
        }else{
            sender.title = "Online"
            if Utility.isNetworkAvailable(controller: self){
                self.getUsersList()
            }
        }
    }
    
    @objc func sort(){
        
        let selectedAlmanacEntries = itemRecords.sorted {
            var isSorted = false
            if let first = $0.type, let second = $1.type {
                isSorted = first < second
            }
            return isSorted
        }
        
        itemRecords.removeAll()
        itemRecords = selectedAlmanacEntries
        
        TableView.reloadData()
    }
    
    
    func configureTable(){
        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        
        view.addSubview(TableView)
        
        TableView.translatesAutoresizingMaskIntoConstraints = false
        
        TableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
        TableView.leftAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leftAnchor).isActive = true
        TableView.rightAnchor.constraint(equalTo:view.safeAreaLayoutGuide.rightAnchor).isActive = true
        TableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        TableView.dataSource = self
        TableView.delegate = self
        
        TableView.register(TableViewCell.self, forCellReuseIdentifier: "Cell")
        
    }
 }
 extension ViewController{
    // web API call
    
    func getUsersList() {
        self.showLoader()
        guard let url = URL(string: "https://raw.githubusercontent.com/AxxessTech/Mobile-Projects/master/challenge.json") else { return }
        
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            
            do{
                if let data = results.data {
                    let items = try JSONDecoder().decode([ItemObject].self, from: data)
                    
                    
                    DispatchQueue.main.async {
                        self.hideLoader()
                        self.itemRecords = items
                        
                        self.write(arr: self.itemRecords)
                        self.TableView.reloadData()
                        
                    }
                }
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }
 }
 
 
 extension ViewController{
    
    // using for offline realm data base
    
    private func write(arr: [ItemObject]) {
        
        var ArrObj = [ItemObj]()
        
        for  obj in arr {
            let item = ItemObj.create(withId: obj.id ?? "", withType: obj.type ?? "", withDate: obj.date ?? "", withData: obj.data ?? "")
            ArrObj.append(item)
        }
        
        let records = Item.create(ItemObj: ArrObj)
        
        
        do {
            if let realm = try? Realm() {
                try? realm.write {
                    realm.add(records)
                }
            }
        }
        
    }
    
    
    private func read() {
        self.showLoader()
        // Read from Realm
        print("Read from Realm")
        self.itemRecords.removeAll()
        var ArrObj = [ItemObject]()
        
        do {
            if let realm = try? Realm() {
                
                let data = realm.objects(Item.self)
                
                for obj in data{
                    print(obj.objectArr)
                    for currentObj in obj.objectArr{
                        ArrObj.append(ItemObject(id: currentObj.id, type: currentObj.type, date: currentObj.date, data: currentObj.data))
                    }
                }
                
            }
        }
        self.itemRecords = ArrObj
        
        DispatchQueue.main.async {
            self.hideLoader()
            self.TableView.reloadData()
            
        }
    }
 }
 
 extension ViewController:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        
        cell.currentObject = itemRecords[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let toController = UIViewController()
        
        toController.view.backgroundColor = .white
        
        let profileImageView:UIImageView = {
            let img = UIImageView()
            //img.frame = img.contentClippingRect
            img.contentMode = .scaleAspectFill
            img.translatesAutoresizingMaskIntoConstraints = false  
            img.layer.cornerRadius = 5
            img.clipsToBounds = true
            img.center = toController.view.center
            img.frame.size.width = 200
            img.frame.size.height = 200
            
            return img
        }()
        
        
        
        profileImageView.frame = profileImageView.contentClippingRect
        
        toController.view.addSubview(profileImageView)
        
        
        if let name = itemRecords[indexPath.row].data {
            var imageURL: URL?
            if name.contains("https"){
                imageURL = URL(string: name)
            }else{
                imageURL = URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/600px-No_image_available.svg.png")
            }
            profileImageView.sd_setImage(with: imageURL)
            
        }
        
        
        self.navigationController?.pushViewController(toController, animated: true)
        
        
        //        self.present(toController, animated: true, completion: nil)
        
        
    }
    
 }
 

